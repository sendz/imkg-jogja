/*
  Warnings:

  - You are about to drop the `Participant` table. If the table is not empty, all the data it contains will be lost.
  - You are about to drop the column `participantId` on the `EventTicket` table. All the data in the column will be lost.
  - You are about to drop the column `paymentId` on the `EventTicket` table. All the data in the column will be lost.
  - Added the required column `userId` to the `EventTicket` table without a default value. This is not possible if the table is not empty.

*/
-- DropIndex
DROP INDEX "Participant_email_key";

-- DropIndex
DROP INDEX "Participant_phone_key";

-- DropTable
PRAGMA foreign_keys=off;
DROP TABLE "Participant";
PRAGMA foreign_keys=on;

-- CreateTable
CREATE TABLE "Payment" (
    "id" TEXT NOT NULL PRIMARY KEY,
    "approvalCode" TEXT NOT NULL,
    "bank" TEXT NOT NULL,
    "cardType" TEXT NOT NULL,
    "fraudStatus" TEXT NOT NULL,
    "grossAmount" REAL NOT NULL,
    "maskedCard" TEXT,
    "orderId" TEXT NOT NULL,
    "paymentType" TEXT NOT NULL,
    "statusMessage" TEXT NOT NULL,
    "transactionId" TEXT NOT NULL,
    "transactionStatus" TEXT NOT NULL,
    "transactionTime" DATETIME NOT NULL
);

-- RedefineTables
PRAGMA foreign_keys=OFF;
CREATE TABLE "new_User" (
    "id" TEXT NOT NULL PRIMARY KEY,
    "email" TEXT NOT NULL,
    "phone" TEXT,
    "name" TEXT DEFAULT 'Anonymous',
    "role" TEXT NOT NULL DEFAULT 'Member',
    "createdAt" DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "updatedAt" DATETIME NOT NULL
);
INSERT INTO "new_User" ("createdAt", "email", "id", "name", "role", "updatedAt") SELECT "createdAt", "email", "id", "name", "role", "updatedAt" FROM "User";
DROP TABLE "User";
ALTER TABLE "new_User" RENAME TO "User";
CREATE UNIQUE INDEX "User_email_key" ON "User"("email");
CREATE UNIQUE INDEX "User_phone_key" ON "User"("phone");
CREATE TABLE "new_EventTicket" (
    "id" TEXT NOT NULL PRIMARY KEY,
    "price" REAL NOT NULL,
    "paymentStatus" TEXT DEFAULT 'Pending',
    "issued" BOOLEAN NOT NULL DEFAULT false,
    "checkedIn" BOOLEAN NOT NULL DEFAULT false,
    "createdAt" DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "updatedAt" DATETIME NOT NULL,
    "eventId" TEXT NOT NULL,
    "userId" TEXT NOT NULL,
    CONSTRAINT "EventTicket_eventId_fkey" FOREIGN KEY ("eventId") REFERENCES "Event" ("id") ON DELETE CASCADE ON UPDATE CASCADE,
    CONSTRAINT "EventTicket_userId_fkey" FOREIGN KEY ("userId") REFERENCES "User" ("id") ON DELETE CASCADE ON UPDATE CASCADE
);
INSERT INTO "new_EventTicket" ("createdAt", "eventId", "id", "paymentStatus", "price", "updatedAt") SELECT "createdAt", "eventId", "id", "paymentStatus", "price", "updatedAt" FROM "EventTicket";
DROP TABLE "EventTicket";
ALTER TABLE "new_EventTicket" RENAME TO "EventTicket";
PRAGMA foreign_key_check;
PRAGMA foreign_keys=ON;
