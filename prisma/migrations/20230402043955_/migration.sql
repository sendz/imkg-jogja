/*
  Warnings:

  - You are about to drop the column `userId` on the `EventTicket` table. All the data in the column will be lost.
  - Added the required column `participantId` to the `EventTicket` table without a default value. This is not possible if the table is not empty.

*/
-- CreateTable
CREATE TABLE "Participant" (
    "id" TEXT NOT NULL PRIMARY KEY,
    "name" TEXT NOT NULL,
    "phone" TEXT NOT NULL,
    "email" TEXT NOT NULL
);

-- RedefineTables
PRAGMA foreign_keys=OFF;
CREATE TABLE "new_EventTicket" (
    "id" TEXT NOT NULL PRIMARY KEY,
    "paymentId" TEXT NOT NULL DEFAULT 'pending',
    "paymentStatus" TEXT NOT NULL DEFAULT 'pending',
    "price" REAL NOT NULL,
    "createdAt" DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "updatedAt" DATETIME NOT NULL,
    "participantId" TEXT NOT NULL,
    "eventId" TEXT NOT NULL,
    CONSTRAINT "EventTicket_participantId_fkey" FOREIGN KEY ("participantId") REFERENCES "Participant" ("id") ON DELETE CASCADE ON UPDATE CASCADE,
    CONSTRAINT "EventTicket_eventId_fkey" FOREIGN KEY ("eventId") REFERENCES "Event" ("id") ON DELETE CASCADE ON UPDATE CASCADE
);
INSERT INTO "new_EventTicket" ("createdAt", "eventId", "id", "paymentId", "paymentStatus", "price", "updatedAt") SELECT "createdAt", "eventId", "id", "paymentId", "paymentStatus", "price", "updatedAt" FROM "EventTicket";
DROP TABLE "EventTicket";
ALTER TABLE "new_EventTicket" RENAME TO "EventTicket";
CREATE TABLE "new_User" (
    "id" TEXT NOT NULL PRIMARY KEY,
    "email" TEXT NOT NULL,
    "name" TEXT DEFAULT 'Anonymous',
    "role" TEXT NOT NULL DEFAULT 'Author',
    "createdAt" DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "updatedAt" DATETIME NOT NULL
);
INSERT INTO "new_User" ("createdAt", "email", "id", "name", "role", "updatedAt") SELECT "createdAt", "email", "id", "name", "role", "updatedAt" FROM "User";
DROP TABLE "User";
ALTER TABLE "new_User" RENAME TO "User";
CREATE UNIQUE INDEX "User_email_key" ON "User"("email");
CREATE TABLE "new_Event" (
    "id" TEXT NOT NULL PRIMARY KEY,
    "title" TEXT NOT NULL,
    "startDate" DATETIME,
    "endDate" DATETIME,
    "location" TEXT,
    "locationDescription" TEXT,
    "latLong" TEXT,
    "imageHeader" TEXT,
    "body" TEXT NOT NULL,
    "price" REAL DEFAULT 0,
    "category" TEXT DEFAULT 'Event',
    "createdAt" DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "updatedAt" DATETIME NOT NULL,
    "userId" TEXT NOT NULL,
    CONSTRAINT "Event_userId_fkey" FOREIGN KEY ("userId") REFERENCES "User" ("id") ON DELETE CASCADE ON UPDATE CASCADE
);
INSERT INTO "new_Event" ("body", "createdAt", "endDate", "id", "imageHeader", "latLong", "location", "locationDescription", "price", "startDate", "title", "updatedAt", "userId") SELECT "body", "createdAt", "endDate", "id", "imageHeader", "latLong", "location", "locationDescription", "price", "startDate", "title", "updatedAt", "userId" FROM "Event";
DROP TABLE "Event";
ALTER TABLE "new_Event" RENAME TO "Event";
PRAGMA foreign_key_check;
PRAGMA foreign_keys=ON;
