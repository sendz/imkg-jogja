import { fetch } from "@remix-run/node";
import type { User } from "@prisma/client";

const { env } = require("../utils/env");

const headers = {
  Accept: "application/json",
  "Content-Type": "application/json",
  Authorization:
    "Basic " + Buffer.from(env.MIDTRANS_SERVER_KEY + ":").toString("base64"),
};

export const createTransaction = async (
  orderId: string,
  amount: number,
  customer: User
) => {
  return fetch(env.MIDTRANS_API_HOST, {
    method: "POST",
    headers,
    body: JSON.stringify({
      transaction_details: {
        order_id: orderId,
        gross_amount: amount,
      },
      customer_details: {
        first_name: customer.name?.split(" ")[0],
        last_name: customer.name?.split(" ")[1] || "",
        email: customer.email || "",
        phone: customer.phone || "",
      },
    }),
  }).then((res) => res.json());
};
