import { requireUserId } from "~/session.server";
import { json, LoaderArgs } from "@remix-run/node";
import { getEventListItemsByUser } from "~/models/event.server";
import { NavLink, Outlet, useLoaderData } from "@remix-run/react";

export async function loader({ request }: LoaderArgs) {
  const userId = await requireUserId(request);
  const events = await getEventListItemsByUser({ userId });
  return json({ events });
}

export default function DashboardEventsPage() {
  const data = useLoaderData<typeof loader>();
  return (
    <div>
      <div className="flex flex-row">
        <h1 className="text-3xl">Events</h1>
        <NavLink to={"add"} className="ml-4">
          <button className="rounded bg-blue-500 px-4 py-2 font-bold text-white hover:bg-blue-700">
            New Event
          </button>
        </NavLink>
      </div>
      <main>
        <Outlet />
      </main>
    </div>
  );
}
