import { json, LoaderArgs, V2_MetaFunction } from "@remix-run/node";
import { getTicketById } from "~/models/ticket.server";
import { requireUserId } from "~/session.server";
import { useLoaderData } from "@remix-run/react";
import { NavigationBar } from "~/components/NavigationBar";
import { CalendarIcon, MapPinIcon } from "@heroicons/react/24/outline";
import moment from "moment/moment";
import { TicketQr } from "~/components/TicketQr";
import { MarkdownLoader } from "~/components/MarkdownLoader";
import { MapViewer } from "~/components/MapViewer";

export async function loader({ request, params }: LoaderArgs) {
  const { ticketId } = params;
  const userId = await requireUserId(request);
  const ticket = await getTicketById({ id: ticketId!, userId });

  return json({
    ticket,
    env: {
      GMAPS_API_KEY: process.env.GMAPS_API_KEY,
    },
  });
}

export const meta: V2_MetaFunction = () => [
  { title: "IMKG Yogyakarta - Event Ticket" },
];
export default function TicketDetailPage() {
  const data = useLoaderData<typeof loader>();
  return (
    <>
      <NavigationBar />
      <div className="bg-white py-24 sm:py-32">
        <div className="mx-auto max-w-7xl px-6 lg:px-8">
          <div className="mx-auto lg:mx-0">
            <h2 className="text-3xl font-bold tracking-tight text-gray-900 sm:text-4xl">
              {data.ticket?.event?.title}
            </h2>
            <time
              className="my-8 flex flex-row"
              dateTime={`${data.ticket?.event?.startDate}-${data.ticket?.event?.endDate}`}
            >
              <CalendarIcon className="h-5 w-5" />
              <span className="ml-2">
                {moment(data.ticket?.event?.startDate).format(
                  "DD MMM YYYY HH:MM"
                )}{" "}
                -{" "}
                {moment(data.ticket?.event?.endDate).format(
                  "DD MMM YYYY HH:MM"
                )}
              </span>
            </time>
            <div>
              <div className="text-m my-4 text-gray-900">
                Payment Status: {data.ticket?.paymentStatus}
              </div>
              <address className="flex-start my-4 flex flex-row">
                <MapPinIcon className="h-5 w-5" />
                <div className="ml-2">
                  <p className="font-bold">{data.ticket?.event?.location}</p>
                  {data.ticket?.event?.locationDescription && (
                    <MarkdownLoader>
                      {data.ticket?.event?.locationDescription}
                    </MarkdownLoader>
                  )}
                </div>
              </address>
              {data.ticket?.event?.latLong && (
                <div className="my-4">
                  <MapViewer
                    coordinate={{
                      lat: Number.parseFloat(
                        data.ticket?.event?.latLong.split(",")[0]
                      ),
                      lng: Number.parseFloat(
                        data.ticket?.event?.latLong.split(",")[1]
                      ),
                    }}
                    mapsApiKey={data.env.GMAPS_API_KEY!}
                  />
                </div>
              )}
            </div>
            <h3 className="text-xl font-bold tracking-tight text-gray-900 sm:text-2xl">
              Your Ticket
            </h3>
            <div className="flex flex-row justify-center">
              <div className="w-1/2">
                <TicketQr
                  ticketId={data.ticket?.id!}
                  eventId={data.ticket?.eventId!}
                  userId={data.ticket?.userId!}
                  paymentStatus={data.ticket?.paymentStatus!}
                />
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
}
