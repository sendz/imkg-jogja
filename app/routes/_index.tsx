import type { LoaderArgs, V2_MetaFunction } from "@remix-run/node";
import { Banner } from "~/components/Banner";
import { NavigationBar } from "~/components/NavigationBar";
import { Event } from "~/components/Event";
import { getEventListItems } from "~/models/event.server";
import { json } from "@remix-run/node";
import { Gallery } from "~/components/Gallery";

export const meta: V2_MetaFunction = () => [{ title: "IMKG Yogyakarta" }];

export async function loader({ request }: LoaderArgs) {
  const events = await getEventListItems(3);
  return json({ events });
}
export default function Index() {
  return (
    <main className="scroll-smooth bg-white">
      <NavigationBar />
      <Banner />
      <Event />
      <Gallery />
    </main>
  );
}
