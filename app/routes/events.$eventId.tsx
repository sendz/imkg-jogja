import { json, LoaderArgs, V2_MetaFunction } from "@remix-run/node";
import { getEvent } from "~/models/event.server";
import { Outlet, useLoaderData } from "@remix-run/react";
import { NavigationBar } from "~/components/NavigationBar";
import { Banner } from "~/components/Banner";
import { CalendarIcon } from "@heroicons/react/24/outline";
import moment from "moment";

export const meta: V2_MetaFunction = () => [{ title: "IMKG Yogyakarta" }];

export async function loader({ params }: LoaderArgs) {
  const { eventId } = params;
  const event = await getEvent({ id: eventId! });
  return json({
    event,
  });
}

export default function EventDetailPage() {
  const data = useLoaderData<typeof loader>();
  return (
    <>
      <NavigationBar />
      {data.event?.imageHeader && (
        <Banner image={data.event.imageHeader} short={true} />
      )}
      <div className="bg-white py-24 sm:py-32">
        <div className="mx-auto max-w-7xl px-6 lg:px-8">
          <div className="mx-auto lg:mx-0">
            <h2 className="text-3xl font-bold tracking-tight text-gray-900 sm:text-4xl">
              {data.event?.title}
            </h2>
            <time
              className="my-8 flex flex-row"
              dateTime={`${data.event?.startDate}-${data.event?.endDate}`}
            >
              <CalendarIcon className="h-5 w-5" />
              <span className="ml-2">
                {moment(data.event?.startDate).format("DD MMM YYYY HH:MM")} -{" "}
                {moment(data.event?.endDate).format("DD MMM YYYY HH:MM")}
              </span>
            </time>
            <Outlet />
          </div>
        </div>
      </div>
    </>
  );
}
