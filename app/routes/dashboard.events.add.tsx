import type { ActionArgs } from "@remix-run/node";
import { json, redirect } from "@remix-run/node";
import { requireUserId } from "~/session.server";
import { createEvent } from "~/models/event.server";
import { Form, useActionData, useLoaderData } from "@remix-run/react";
import React, { useState } from "react";
import moment from "moment";
import { EVENT_CATEGORY } from "~/constants/EVENT_CATEGORY";
import type { ICoordinate } from "~/components/MapPicker";
import { MapPicker } from "~/components/MapPicker";
import { env } from "~/utils/env";

export async function loader() {
  const { GMAPS_API_KEY } = env;
  return json({
    env: {
      GMAPS_API_KEY,
    },
  });
}

export async function action({ request }: ActionArgs) {
  const userId = await requireUserId(request);

  const formData = await request.formData();

  const title = formData.get("title");
  const body = formData.get("body");
  const startDate = moment(
    formData.get("startDate") as unknown as string
  ).toDate();
  const endDate = moment(formData.get("endDate") as unknown as string).toDate();
  const location = formData.get("location") as unknown as string;
  const locationDescription = formData.get(
    "locationDescription"
  ) as unknown as string;
  const latLong = formData.get("latLong") as unknown as string;
  const imageHeader = formData.get("imageHeader") as unknown as string;
  const price = Number.parseFloat(formData.get("price") as unknown as string);
  const category = formData.get("category") as unknown as string;

  console.log("START DATE: ", startDate.valueOf());
  console.log("END DATE: ", endDate.valueOf());
  if (typeof title !== "string" || title.length === 0) {
    return json(
      {
        errors: {
          title: "Title is required",
          body: null,
          startDate: null,
          endDate: null,
        },
      },
      { status: 400 }
    );
  }

  if (typeof body !== "string" || body.length === 0) {
    return json(
      {
        errors: {
          title: null,
          body: "Body is required",
          startDate: null,
          endDate: null,
        },
      },
      { status: 400 }
    );
  }

  if (
    typeof startDate !== "object" ||
    Number.isNaN(startDate.valueOf()) ||
    startDate.toDateString().length === 0
  ) {
    return json(
      {
        errors: {
          title: null,
          body: null,
          startDate: "Start date is required",
          endDate: null,
        },
      },
      { status: 400 }
    );
  }

  if (
    typeof endDate !== "object" ||
    Number.isNaN(endDate.valueOf()) ||
    endDate.toDateString().length === 0
  ) {
    return json(
      {
        errors: {
          title: null,
          body: null,
          startDate: null,
          endDate: "End date is required",
        },
      },
      { status: 400 }
    );
  }

  const event = await createEvent({
    endDate,
    imageHeader,
    latLong,
    location,
    locationDescription,
    price,
    startDate,
    title,
    body,
    userId,
    category,
  });

  return redirect(`/dashboard/events`);
  // return redirect(`/events/${event.id}`);
}

export default function NewEventPage() {
  const data = useLoaderData<typeof loader>();
  const actionData = useActionData<typeof action>();
  const [coordinate, setCoordinate] = useState<ICoordinate>();
  const titleRef = React.useRef<HTMLInputElement>(null);
  const bodyRef = React.useRef<HTMLTextAreaElement>(null);
  const startDateRef = React.useRef<HTMLInputElement>(null);
  const endDateRef = React.useRef<HTMLInputElement>(null);
  const locationRef = React.useRef<HTMLInputElement>(null);
  const locationDescriptionRef = React.useRef<HTMLTextAreaElement>(null);
  const latLongRef = React.useRef<HTMLInputElement>(null);
  const imageHeaderRef = React.useRef<HTMLInputElement>(null);
  const priceRef = React.useRef<HTMLInputElement>(null);
  const categoryRef = React.useRef<HTMLSelectElement>(null);

  React.useEffect(() => {
    if (actionData?.errors?.title) {
      titleRef.current?.focus();
    } else if (actionData?.errors?.body) {
      bodyRef.current?.focus();
    } else if (actionData?.errors?.startDate) {
      bodyRef.current?.focus();
    } else if (actionData?.errors?.endDate) {
      bodyRef.current?.focus();
    }
  }, [actionData]);

  return (
    <Form
      method="post"
      style={{
        display: "flex",
        flexDirection: "column",
        gap: 8,
        width: "100%",
      }}
    >
      <div>
        <label className="flex w-full flex-col gap-1">
          <span>Title: </span>
          <input
            ref={titleRef}
            name="title"
            className="flex-1 rounded-md border-2 border-blue-500 px-3 text-lg leading-loose"
            aria-invalid={actionData?.errors?.title ? true : undefined}
            aria-errormessage={
              actionData?.errors?.title ? "title-error" : undefined
            }
          />
        </label>
        {actionData?.errors?.title && (
          <div className="pt-1 text-red-700" id="title-error">
            {actionData.errors.title}
          </div>
        )}
      </div>

      <div>
        <label className="flex w-full flex-col gap-1">
          <span>Category: </span>
          <select
            ref={categoryRef}
            name="category"
            className="flex-1 rounded-md border-2 border-blue-500 px-3 text-lg leading-loose"
            style={{ height: 40 }}
          >
            {EVENT_CATEGORY.map((category, index) => (
              <option key={index} value={category}>
                {category}
              </option>
            ))}
          </select>
        </label>
      </div>

      <div>
        <label className="flex w-full flex-col gap-1">
          <span>Image Header: </span>
          <input
            ref={imageHeaderRef}
            name="imageHeader"
            className="flex-1 rounded-md border-2 border-blue-500 px-3 text-lg leading-loose"
            type="url"
            placeholder="URL of the image"
          />
        </label>
      </div>

      <div>
        <label className="flex w-full flex-col gap-1">
          <span>Body: (Markdown supported)</span>
          <textarea
            ref={bodyRef}
            name="body"
            rows={8}
            className="w-full flex-1 rounded-md border-2 border-blue-500 px-3 py-2 text-lg leading-6"
            aria-invalid={actionData?.errors?.body ? true : undefined}
            aria-errormessage={
              actionData?.errors?.body ? "body-error" : undefined
            }
          />
        </label>
        {actionData?.errors?.body && (
          <div className="pt-1 text-red-700" id="body-error">
            {actionData.errors.body}
          </div>
        )}
      </div>

      <div className="flex flex-row">
        <div className="w-1/2">
          <label className="flex w-full flex-col gap-1">
            <span>Start Date: </span>
            <input
              ref={startDateRef}
              name="startDate"
              className="flex-1 rounded-md border-2 border-blue-500 px-3 text-lg leading-loose"
              type="datetime-local"
              aria-invalid={actionData?.errors?.startDate ? true : undefined}
              aria-errormessage={
                actionData?.errors?.startDate ? "body-error" : undefined
              }
            />
          </label>

          {actionData?.errors?.startDate && (
            <div className="pt-1 text-red-700" id="title-error">
              {actionData.errors.startDate}
            </div>
          )}
        </div>
        <div className="w-1/2">
          <label className="flex w-full flex-col gap-1">
            <span>End Date: </span>
            <input
              ref={endDateRef}
              name="endDate"
              className="flex-1 rounded-md border-2 border-blue-500 px-3 text-lg leading-loose"
              type="datetime-local"
              aria-invalid={actionData?.errors?.endDate ? true : undefined}
              aria-errormessage={
                actionData?.errors?.endDate ? "body-error" : undefined
              }
            />
          </label>
          {actionData?.errors?.endDate && (
            <div className="pt-1 text-red-700" id="title-error">
              {actionData.errors.endDate}
            </div>
          )}
        </div>
      </div>

      <div>
        <label className="flex w-full flex-col gap-1">
          <span>Location: </span>
          <input
            type="text"
            ref={locationRef}
            name="location"
            className="flex-1 rounded-md border-2 border-blue-500 px-3 text-lg leading-loose"
          />
        </label>
      </div>

      <div>
        <label className="flex w-full flex-col gap-1">
          <span>Location Details: (Markdown supported)</span>
          <textarea
            ref={locationDescriptionRef}
            name="locationDescription"
            rows={8}
            className="w-full flex-1 rounded-md border-2 border-blue-500 px-3 py-2 text-lg leading-6"
          />
        </label>
      </div>

      <div>
        <label className="flex w-full flex-col gap-1">
          <span>Map Location: </span>
          <input
            type="hidden"
            ref={latLongRef}
            name="latLong"
            readOnly
            value={`${coordinate?.lat},${coordinate?.lng}`}
            className="flex-1 rounded-md border-2 border-blue-500 px-3 text-lg leading-loose"
          />
        </label>
        {data.env.GMAPS_API_KEY && (
          <MapPicker
            onChange={setCoordinate}
            mapsApiKey={data.env.GMAPS_API_KEY}
          />
        )}
      </div>

      <div>
        <label className="flex w-full flex-col gap-1">
          <span>Price: (in IDR)</span>
          <input
            type="number"
            ref={priceRef}
            name="price"
            defaultValue={0}
            className="flex-1 rounded-md border-2 border-blue-500 px-3 text-lg leading-loose"
          />
        </label>
      </div>

      <div className="text-right">
        <button
          type="submit"
          className="rounded bg-blue-500 px-4 py-2 font-bold text-white hover:bg-blue-700"
        >
          Save
        </button>
      </div>
    </Form>
  );
}
