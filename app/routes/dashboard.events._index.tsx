import { getEventListItemsByUser } from "~/models/event.server";
import { requireUserId } from "~/session.server";
import { json, LoaderArgs } from "@remix-run/node";
import { NavLink, useLoaderData } from "@remix-run/react";
import moment from "moment";

export async function loader({ request }: LoaderArgs) {
  const userId = await requireUserId(request);
  const events = await getEventListItemsByUser({ userId });
  return json({ events });
}

export default function DashboardEventIndex() {
  const data = useLoaderData<typeof loader>();
  return (
    <section>
      <table>
        <thead>
          <tr>
            <th>Name</th>
            <th>Dates</th>
            <th>Location</th>
            <th>Actions</th>
          </tr>
        </thead>
        <tbody>
          {data.events.map((event) => (
            <tr key={event.id}>
              <td>{event.title}</td>
              <td>
                {moment(event.startDate).toLocaleString()} -{" "}
                {moment(event.endDate).toLocaleString()}
              </td>
              <td>{event.location}</td>
              <td>
                <NavLink to={`edit/${event.id}`}>Edit</NavLink>
              </td>
            </tr>
          ))}
        </tbody>
      </table>
    </section>
  );
}
