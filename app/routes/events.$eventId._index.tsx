import type { ActionArgs, LoaderArgs, V2_MetaFunction } from "@remix-run/node";
import { json } from "@remix-run/node";
import { getEvent } from "~/models/event.server";
import { Form, NavLink, useLoaderData } from "@remix-run/react";
import { MapPinIcon } from "@heroicons/react/24/outline";
import { MarkdownLoader } from "~/components/MarkdownLoader";
import type { ICoordinate } from "~/components/MapPicker";
import { MapViewer } from "~/components/MapViewer";
import { useOptionalUser } from "~/utils";
import { getUser, requireUserId } from "~/session.server";
import { env } from "~/utils/env";
import { createTransaction } from "~/external-api/midtrans";
import {
  createTicket,
  getTicketByEventId,
  updateTicketStatus,
} from "~/models/ticket.server";
import { formRequest } from "~/utils/formRequest";
import { createPayment } from "~/models/payment.server";
import { TicketQr } from "~/components/TicketQr";

export async function loader({ request, params }: LoaderArgs) {
  const { eventId } = params;

  const user = await getUser(request);
  const event = await getEvent({ id: eventId! });
  const tickets = await getTicketByEventId({
    eventId: event?.id!,
    userId: user?.id!,
  });

  const { GMAPS_API_KEY } = env;

  return json({
    event,
    tickets,
    env: {
      GMAPS_API_KEY,
    },
  });
}

export async function action({ request }: ActionArgs) {
  const userId = await requireUserId(request);
  const user = await getUser(request);

  const formData = await request.formData();
  const eventId = formData.get("eventId") as unknown as string;
  const price = Number.parseFloat(formData.get("price") as unknown as string);
  const intent = formData.get("intent") as unknown as string;

  console.log("action", intent);
  if (intent === "generatePayment") {
    const ticket = await createTicket({
      eventId,
      price,
      userId,
    });
    const midtransResponse = await createTransaction(ticket.id, price, user!);
    console.log("midtransResponse", midtransResponse);
    return json({
      payment: midtransResponse,
      ticket,
    });
  }

  if (intent === "updateTicketStatus") {
    const ticketId = formData.get("ticketId") as unknown as string;
    const approvalCode = formData.get("approvalCode") as unknown as string;
    const bank = formData.get("bank") as unknown as string;
    const cardType = formData.get("cardType") as unknown as string;
    const fraudStatus = formData.get("fraudStatus") as unknown as string;
    const grossAmount = Number.parseFloat(
      formData.get("grossAmount") as unknown as string
    );
    const maskedCard = formData.get("maskedCard") as unknown as string;
    const orderId = formData.get("orderId") as unknown as string;
    const paymentType = formData.get("paymentType") as unknown as string;
    const statusMessage = formData.get("statusMessage") as unknown as string;
    const transactionId = formData.get("transactionId") as unknown as string;
    const transactionStatus = formData.get(
      "transactionStatus"
    ) as unknown as string;
    const transactionTime = new Date(
      formData.get("transactionTime") as unknown as string
    );

    const paymentResult = await createPayment({
      approvalCode,
      bank,
      cardType,
      fraudStatus,
      grossAmount,
      maskedCard,
      orderId,
      paymentType,
      statusMessage,
      transactionId,
      transactionStatus,
      transactionTime,
      userId,
    });

    const ticket = await updateTicketStatus({
      id: ticketId,
      paymentId: paymentResult.id,
      paymentStatus: transactionStatus,
      issued: true,
      checkedIn: false,
      updatedAt: new Date(),
    });

    return json({
      ticket,
    });
  }

  // const ticket = await createTicket({
  //   eventId,
  //   price,
  //   userId,
  // });

  // return redirect(`/tickets/${ticket.id}`);
}

export function shouldRevalidate({
  actionResult,
  defaultShouldRevalidate,
}: any) {
  console.log("shouldRevalidate", actionResult);

  if (actionResult.payment.token) {
    window.snap.pay(actionResult.payment.token, {
      onSuccess: function (result: any) {
        console.log("success", result);
        formRequest("", "post", {
          intent: "updateTicketStatus",
          ticketId: actionResult.ticket.id,
          approvalCode: result.approval_code,
          bank: result.bank,
          cardType: result.card_type,
          fraudStatus: result.fraud_status,
          grossAmount: result.gross_amount,
          maskedCard: result.masked_card,
          orderId: result.order_id,
          paymentType: result.payment_type,
          statusMessage: result.status_message,
          transactionId: result.transaction_id,
          transactionStatus: result.transaction_status,
          transactionTime: result.transaction_time,
        });
      },
      onClose: function () {
        console.log("customer closed the popup without finishing the payment");
      },
      onPending: function (result: any) {
        console.log("success", result);
      },
      onError: function (result: any) {
        console.log("error", result);
      },
    });
  }
}

export const meta: V2_MetaFunction = () => [{ title: "IMKG Yogyakarta" }];
export default function EventDetailPage() {
  const data = useLoaderData<typeof loader>();
  const latLong = data.event?.latLong
    ?.split(",")
    .map((x) => Number.parseFloat(x));
  const coordinate: ICoordinate = {
    lat: latLong?.[0]!,
    lng: latLong?.[1]!,
  };

  const user = useOptionalUser();

  console.log("DATA", data);

  return (
    <>
      <div className="flex h-screen flex-col md:flex-row">
        <section className="w-full md:w-3/5">
          <address className="flex-start my-4 flex flex-row">
            <MapPinIcon className="h-5 w-5" />
            <div className="ml-2">
              <p className="font-bold">{data.event?.location}</p>
              {data.event?.locationDescription && (
                <MarkdownLoader>
                  {data.event?.locationDescription}
                </MarkdownLoader>
              )}
            </div>
          </address>
          {latLong && coordinate && data.env.GMAPS_API_KEY && (
            <div className="my-8">
              <MapViewer
                coordinate={coordinate}
                mapsApiKey={data.env.GMAPS_API_KEY}
              />
            </div>
          )}
          <article>
            <MarkdownLoader>{data.event?.body}</MarkdownLoader>
          </article>
        </section>
        <div className="w-full md:w-2/5">
          <div className="-mt-2 p-2 lg:mt-0 lg:w-full lg:max-w-md lg:flex-shrink-0">
            <div className="rounded-2xl bg-gray-50 py-6 text-center ring-1 ring-inset ring-gray-900/5 lg:flex lg:flex-col lg:justify-center lg:py-16">
              {data.tickets?.length > 0 && (
                <>
                  <h4>Your Tickets, click on QR to see details</h4>
                  <div className="mx-auto flex max-w-xs flex-row flex-wrap px-2">
                    {data.tickets?.map((ticket) => (
                      <div className="w-1/2 p-2" key={ticket.id}>
                        <NavLink to={`/tickets/${ticket.id}`}>
                          <TicketQr
                            ticketId={ticket.id}
                            userId={ticket.userId}
                            eventId={ticket.eventId}
                            paymentStatus={ticket.paymentStatus!}
                          />
                        </NavLink>
                      </div>
                    ))}
                  </div>
                </>
              )}
              <div className="mx-auto max-w-xs px-4">
                <p className="text-base font-semibold text-gray-600">
                  {data.event?.title}
                </p>
                <p className="mt-6 flex items-baseline justify-center gap-x-2">
                  <span className="text-5xl font-bold tracking-tight text-gray-900">
                    {data.event?.price?.toLocaleString()}
                  </span>
                  <span className="text-sm font-semibold leading-6 tracking-wide text-gray-600">
                    IDR
                  </span>
                </p>
                {user ? (
                  <Form method="post">
                    <input
                      type="hidden"
                      name="intent"
                      value="generatePayment"
                    />
                    <input
                      type="hidden"
                      name="eventId"
                      value={data.event?.id}
                    />
                    <input type="hidden" name="userId" value={user.id} />
                    <input
                      type="hidden"
                      name="price"
                      value={data.event?.price?.toString()}
                    />
                    <button className="mt-10 block w-full rounded-md bg-indigo-600 px-3 py-2 text-center text-sm font-semibold text-white shadow-sm hover:bg-indigo-500 focus-visible:outline focus-visible:outline-2 focus-visible:outline-offset-2 focus-visible:outline-indigo-600">
                      Reserve your spot
                    </button>
                  </Form>
                ) : (
                  <NavLink
                    to="/join"
                    className="mt-10 block w-full rounded-md bg-indigo-600 px-3 py-2 text-center text-sm font-semibold text-white shadow-sm hover:bg-indigo-500 focus-visible:outline focus-visible:outline-2 focus-visible:outline-offset-2 focus-visible:outline-indigo-600"
                  >
                    Join us
                  </NavLink>
                )}
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
}
