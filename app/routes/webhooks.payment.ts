import type { ActionArgs, LoaderArgs } from "@remix-run/node";
import { json } from "@remix-run/node";
import moment from "moment";
import { IPaymentHooks } from "~/interfaces/Payment.hooks";
import { updatePayment } from "~/models/payment.server";

export const action = async ({ request, params }: ActionArgs) => {
  if (request.method !== "POST") {
    return json({ error: "Method not allowed" }, { status: 405 });
  }

  try {
    const data: IPaymentHooks = await request.json();
    console.log(data);

    updatePayment({
      approvalCode: data.approval_code!,
      acquirer: data.acquirer!,
      bank: data.bank! || data.va_numbers![0].bank!,
      cardType: data.card_type!,
      fraudStatus: data.fraud_status!,
      eci: data.eci!,
      currency: data.currency!,
      grossAmount: Number.parseFloat(data.gross_amount!),
      issuer: data.issuer!,
      maskedCard: data.masked_card!,
      merchantId: data.merchant_id!,
      orderId: data.order_id!,
      paymentType: data.payment_type!,
      permataVaNumber: data.permata_va_number!,
      vaNumbers: data.va_numbers!.join(","),
      paymentAmounts: data
        .payment_amounts!.map(({ amount }) => amount)
        .join("; "),
      settlementTime: moment(
        data.settlement_time!,
        "YYYY-MM-DD HH:MM:SS"
      ).toDate(),
      signatureKey: data.signature_key!,
      statusMessage: data.status_message!,
      statusCode: data.status_code!,
      transactionId: data.transaction_id!,
      transactionStatus: data.transaction_status!,
      transactionTime: moment(
        data.transaction_time!,
        "YYYY-MM-DD HH:MM:SS"
      ).toDate(),
      transactionType: data.transaction_type!,
    });

    return json({ success: true }, 200);
  } catch (error) {
    return json({ error: "Invalid JSON" }, { status: 400 });
  }
};

export const loader = async ({ request, params }: LoaderArgs) => {
  return json({ message: "Webhook is Online" }, { status: 200 });
};
