import { prisma } from "~/db.server";
import type { EventTicket, Payment } from ".prisma/client";
import type { User } from "@prisma/client";

export function createTicket({
  price,
  eventId,
  userId,
}: Pick<EventTicket, "price" | "eventId"> & {
  userId: User["id"];
}) {
  return prisma.eventTicket.create({
    data: {
      paymentStatus: "pending",
      price,
      event: {
        connect: {
          id: eventId,
        },
      },
      user: {
        connect: {
          id: userId,
        },
      },
    },
  });
}

export function getTicketById({
  id,
  userId,
}: Pick<EventTicket, "id"> & { userId: User["id"] }) {
  return prisma.eventTicket.findFirst({
    where: { id, userId },
    include: {
      event: true,
    },
  });
}

export function getTicketByEventId({
  eventId,
  userId,
}: Pick<EventTicket, "eventId"> & { userId: User["id"] }) {
  return prisma.eventTicket.findMany({
    where: { eventId, userId },
    include: {
      event: true,
    },
  });
}

export function getTicketByUserId({ userId }: { userId: User["id"] }) {
  return prisma.eventTicket.findMany({
    where: { userId },
    include: {
      event: true,
    },
  });
}

export function updateTicketStatus({
  id,
  paymentStatus,
  issued,
  checkedIn,
  updatedAt = new Date(),
  paymentId,
}: Pick<
  EventTicket,
  "id" | "paymentStatus" | "issued" | "checkedIn" | "updatedAt"
> & {
  paymentId: Payment["id"];
}) {
  return prisma.eventTicket.update({
    where: { id },
    data: {
      paymentStatus,
      issued,
      checkedIn,
      updatedAt,
      payment: {
        connect: {
          id: paymentId,
        },
      },
    },
  });
}
