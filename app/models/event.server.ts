import type { User, Event } from "@prisma/client";
import { prisma } from "~/db.server";

export function getEvent({ id }: Pick<Event, "id">) {
  return prisma.event.findFirst({
    select: {
      id: true,
      body: true,
      title: true,
      startDate: true,
      endDate: true,
      location: true,
      locationDescription: true,
      latLong: true,
      imageHeader: true,
      price: true,
      category: true,
    },
    where: { id },
  });
}

export function getEventListItems(limit?: number) {
  return prisma.event.findMany({
    take: limit,
    select: {
      id: true,
      title: true,
      startDate: true,
      endDate: true,
      location: true,
      locationDescription: true,
      latLong: true,
      imageHeader: true,
      price: true,
      user: true,
      body: true,
      category: true,
    },
    orderBy: { startDate: "desc" },
  });
}

export function getEventByUser({
  id,
  userId,
}: Pick<Event, "id"> & {
  userId: User["id"];
}) {
  return prisma.event.findFirst({
    select: {
      id: true,
      body: true,
      title: true,
      startDate: true,
      endDate: true,
      location: true,
      locationDescription: true,
      latLong: true,
      imageHeader: true,
      price: true,
      category: true,
    },
    where: { id, userId },
  });
}

export function getEventListItemsByUser({ userId }: { userId: User["id"] }) {
  return prisma.event.findMany({
    select: {
      id: true,
      title: true,
      startDate: true,
      endDate: true,
      location: true,
      locationDescription: true,
      latLong: true,
      imageHeader: true,
      price: true,
      user: true,
      category: true,
    },
    where: { userId },
    orderBy: { startDate: "desc" },
  });
}

export function createEvent({
  body,
  title,
  startDate,
  endDate,
  location,
  locationDescription,
  latLong,
  imageHeader,
  price,
  userId,
  category,
}: Pick<
  Event,
  | "body"
  | "title"
  | "startDate"
  | "endDate"
  | "location"
  | "locationDescription"
  | "latLong"
  | "imageHeader"
  | "price"
  | "category"
> & {
  userId: User["id"];
}) {
  return prisma.event.create({
    data: {
      title,
      body,
      startDate,
      endDate,
      location,
      locationDescription,
      latLong,
      imageHeader,
      price,
      category,
      user: {
        connect: {
          id: userId,
        },
      },
    },
  });
}

export function deleteEvent({
  id,
  userId,
}: Pick<Event, "id"> & { userId: User["id"] }) {
  return prisma.event.deleteMany({
    where: { id, userId },
  });
}
