import type { Payment } from ".prisma/client";
import type { User } from "@prisma/client";
import { prisma } from "~/db.server";

export function createPayment({
  approvalCode,
  bank,
  cardType,
  fraudStatus,
  grossAmount,
  maskedCard,
  orderId,
  paymentType,
  statusMessage,
  transactionId,
  transactionStatus,
  transactionTime,
  userId,
}: Pick<
  Payment,
  | "approvalCode"
  | "bank"
  | "cardType"
  | "fraudStatus"
  | "grossAmount"
  | "maskedCard"
  | "orderId"
  | "paymentType"
  | "statusMessage"
  | "transactionId"
  | "transactionStatus"
  | "transactionTime"
> & {
  userId: User["id"];
}) {
  return prisma.payment.create({
    data: {
      approvalCode,
      bank,
      cardType,
      fraudStatus,
      grossAmount,
      maskedCard,
      orderId,
      paymentType,
      statusMessage,
      transactionId,
      transactionStatus,
      transactionTime,
      user: {
        connect: {
          id: userId,
        },
      },
    },
  });
}

export function updatePayment({
  approvalCode,
  acquirer,
  bank,
  cardType,
  fraudStatus,
  eci,
  currency,
  grossAmount,
  issuer,
  maskedCard,
  merchantId,
  orderId,
  paymentType,
  permataVaNumber,
  vaNumbers,
  paymentAmounts,
  settlementTime,
  signatureKey,
  statusMessage,
  statusCode,
  transactionId,
  transactionStatus,
  transactionTime,
  transactionType,
}: Pick<
  Payment,
  | "approvalCode"
  | "acquirer"
  | "bank"
  | "cardType"
  | "fraudStatus"
  | "eci"
  | "currency"
  | "grossAmount"
  | "issuer"
  | "maskedCard"
  | "merchantId"
  | "orderId"
  | "paymentType"
  | "permataVaNumber"
  | "vaNumbers"
  | "paymentAmounts"
  | "settlementTime"
  | "signatureKey"
  | "statusMessage"
  | "statusCode"
  | "transactionId"
  | "transactionStatus"
  | "transactionTime"
  | "transactionType"
>) {
  return prisma.payment.update({
    where: {
      orderId,
    },
    data: {
      approvalCode,
      acquirer,
      bank,
      cardType,
      fraudStatus,
      eci,
      currency,
      grossAmount,
      issuer,
      maskedCard,
      merchantId,
      orderId,
      paymentType,
      permataVaNumber,
      vaNumbers,
      paymentAmounts,
      settlementTime,
      signatureKey,
      statusMessage,
      statusCode,
      transactionId,
      transactionStatus,
      transactionTime,
      transactionType,
    },
  });
}
