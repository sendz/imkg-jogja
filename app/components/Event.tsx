import React from "react";
import { NavLink, useLoaderData } from "@remix-run/react";
import type { loader } from "~/routes/_index";
import moment from "moment";
import Gravatar from "react-gravatar";

export const Event: React.FC = () => {
  const data = useLoaderData<typeof loader>();
  return (
    <div className="bg-white py-24 sm:py-32" id="events-container">
      <div className="mx-auto max-w-7xl px-6 lg:px-8">
        <div className="mx-auto max-w-2xl lg:mx-0">
          <h2 className="text-3xl font-bold tracking-tight text-gray-900 sm:text-4xl">
            Events
          </h2>
          <p className="mt-2 text-lg leading-8 text-gray-600">
            Let's gather and have fun
          </p>
        </div>
        <div className="mx-auto mt-10 grid max-w-2xl grid-cols-1 gap-x-8 gap-y-16 border-t border-gray-200 pt-10 sm:mt-16 sm:pt-16 lg:mx-0 lg:max-w-none lg:grid-cols-3">
          {data.events.map((event) => (
            <article
              key={event.id}
              className="flex max-w-xl flex-col items-start justify-between"
            >
              <div className="flex items-center gap-x-4 text-xs">
                <time
                  dateTime={`${moment(event.startDate).format(
                    "DD MMM YYYY HH:MM"
                  )} ${moment(event.endDate).format("DD MMM YYYY HH:MM")}`}
                  className="text-gray-500"
                >
                  {`${moment(event.startDate).format(
                    "DD MMM YYYY HH:MM"
                  )} - ${moment(event.endDate).format("DD MMM YYYY HH:MM")}`}
                </time>
                <span className="relative rounded-full bg-gray-50 px-3 py-1.5 font-medium text-gray-600 hover:bg-gray-100">
                  Event
                </span>
              </div>
              <div className="group relative">
                <h3 className="mt-3 text-lg font-semibold leading-6 text-gray-900 group-hover:text-gray-600">
                  <NavLink to={`events/${event.id}`}>
                    <span className="absolute inset-0" />
                    {event.title}
                  </NavLink>
                </h3>
                <p className="mt-5 line-clamp-3 text-sm leading-6 text-gray-600">
                  {event.body}
                </p>
              </div>
              <div className="relative mt-8 flex items-center gap-x-4">
                <Gravatar
                  email={event.user.email}
                  size={40}
                  className="h-10 w-10 rounded-full bg-gray-50"
                />
                <div className="text-sm leading-6">
                  <p className="font-semibold text-gray-900">
                    <span>
                      <span className="absolute inset-0" />
                      {event.user.name}
                    </span>
                  </p>
                  <p className="text-gray-600">{event.user.role}</p>
                </div>
              </div>
            </article>
          ))}
        </div>
      </div>
    </div>
  );
};
