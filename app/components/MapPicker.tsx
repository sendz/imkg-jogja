import React, { useEffect, useState } from "react";
import {
  Autocomplete,
  GoogleMap,
  LoadScript,
  Marker,
  MarkerF,
} from "@react-google-maps/api";
import MapMouseEvent = google.maps.MapMouseEvent;

const containerStyle = {
  width: "100%",
  height: "600px",
};

export type ICoordinate = {
  lat: number;
  lng: number;
};

type IMapPicker = {
  onChange?: (coordinate?: ICoordinate) => void;
  value?: ICoordinate;
  mapsApiKey: string;
};

export const MapPicker: React.FC<IMapPicker> = (props) => {
  const [libraries] = useState<any>(["drawing", "places", "visualization"]);
  const [_isLoaded, setIsLoaded] = useState(false);
  const [coordinate, setCoordinate] = useState<ICoordinate>();
  const [autocomplete, setAutocomplete] = useState<any>();
  const [_marker, setMarker] = useState<Marker>();

  useEffect(() => {
    props.onChange?.(coordinate);
  }, [coordinate]);

  useEffect(() => {
    if (navigator && navigator.geolocation) {
      navigator.geolocation.getCurrentPosition((position) => {
        setCoordinate({
          lat: position.coords.latitude,
          lng: position.coords.longitude,
        });
      });
    }
  }, []);

  const handleMarkerChange = (event: MapMouseEvent) => {
    setCoordinate({ lat: event.latLng?.lat()!, lng: event.latLng?.lng()! });
  };

  const onAutocompleteChange = () => {
    setCoordinate({
      lat: autocomplete.getPlace().geometry.location.lat(),
      lng: autocomplete.getPlace().geometry.location.lng(),
    });
  };

  const markerOnLoad = (marker: any) => {
    setMarker(marker);
  };

  if (!coordinate) {
    return null;
  }

  return (
    <LoadScript
      googleMapsApiKey="AIzaSyANhXNmGbfPv1ev-Wz8MM4UuHqH40snCpA"
      onLoad={() => setIsLoaded(true)}
      libraries={libraries}
    >
      <GoogleMap
        mapContainerStyle={containerStyle}
        center={props.value || coordinate}
        zoom={15}
      >
        {_isLoaded && (props.value || coordinate) && (
          <MarkerF
            position={props.value || coordinate}
            onLoad={markerOnLoad}
            draggable
            onDragEnd={handleMarkerChange}
          />
        )}

        {props.onChange && (
          <Autocomplete
            onPlaceChanged={onAutocompleteChange}
            onLoad={(payload) => setAutocomplete(payload as any)}
          >
            <input
              type="text"
              placeholder="Customized your placeholder"
              style={{
                boxSizing: `border-box`,
                border: `1px solid transparent`,
                width: `240px`,
                height: `32px`,
                padding: `0 12px`,
                borderRadius: `3px`,
                boxShadow: `0 2px 6px rgba(0, 0, 0, 0.3)`,
                fontSize: `14px`,
                outline: `none`,
                textOverflow: `ellipses`,
                position: "absolute",
                left: "50%",
                marginLeft: "-120px",
              }}
            />
          </Autocomplete>
        )}
      </GoogleMap>
    </LoadScript>
  );
};
