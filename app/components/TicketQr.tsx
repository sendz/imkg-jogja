import React from "react";
import { useLoaderData } from "@remix-run/react";
import type { loader } from "~/routes/tickets.$ticketId";
import QRCode from "react-qr-code";

type ITicketQr = {
  ticketId: string;
  userId: string;
  eventId: string;
  paymentStatus: string;
};

export const TicketQr: React.FC<ITicketQr> = (props) => {
  const { ticketId, userId, eventId, paymentStatus } = props;
  const data = useLoaderData<typeof loader>();
  //
  // if (paymentStatus !== "paid") {
  //   return (
  //     <div>
  //       <h4 className="text-xl">Ticket Not Paid, Please complete payment.</h4>
  //     </div>
  //   );
  // }

  return (
    <>
      <QRCode
        className="h-full w-full"
        value={JSON.stringify({
          ticketId,
          userId,
          eventId,
        })}
      />
    </>
  );
};
