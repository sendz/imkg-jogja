import type React from "react";
import { useRemarkSync } from "react-remark";

type IMarkdownLoader = {
  children?: string;
};
export const MarkdownLoader: React.FC<IMarkdownLoader> = (props) => {
  const { children = "" } = props;

  return useRemarkSync(children);
};
