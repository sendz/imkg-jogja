import type { HTMLProps } from "react";
import React from "react";

type IButton = {
  type?: "button" | "submit" | "reset";
} & HTMLProps<HTMLButtonElement>;

export const Button: React.FC<IButton> = (props) => {
  const { children, type, ...rest } = props;
  return (
    <button
      type={type || "button"}
      className="mt-10 block w-full rounded-md bg-indigo-600 px-3 py-2 text-center text-sm font-semibold text-white shadow-sm hover:bg-indigo-500 focus-visible:outline focus-visible:outline-2 focus-visible:outline-offset-2 focus-visible:outline-indigo-600"
      {...rest}
    >
      {children}
    </button>
  );
};
