import React, { Fragment } from "react";
import { Dialog, Transition } from "@headlessui/react";
import Modal from "~/components/Modal";

export const Gallery: React.FC = () => {
  const [selectedImage, setSelectedImage] = React.useState<string>();

  return (
    <>
      <div className="bg-white py-24 sm:py-32" id="gallery-container">
        <div className="mx-auto max-w-7xl px-6 lg:px-8">
          <div className="mx-auto max-w-2xl lg:mx-0">
            <h2 className="text-3xl font-bold tracking-tight text-gray-900 sm:text-4xl">
              Gallery
            </h2>
            <p className="mt-2 text-lg leading-8 text-gray-600">
              Some of our best shots
            </p>
          </div>
          <section className="overflow-hidden text-neutral-700">
            <div className="container my-8">
              <div className="-m-1 flex flex-wrap gap-y-8 md:-m-2">
                <div className="flex w-1/2 flex-wrap">
                  <div className="w-1/2 p-1 md:p-2">
                    <img
                      onClick={() =>
                        setSelectedImage(
                          "https://tecdn.b-cdn.net/img/Photos/Horizontal/Nature/4-col/img%20(70).webp"
                        )
                      }
                      alt="gallery"
                      className="block h-full w-full rounded-lg object-cover object-center"
                      src="https://tecdn.b-cdn.net/img/Photos/Horizontal/Nature/4-col/img%20(70).webp"
                    />
                  </div>
                  <div className="w-1/2 p-1 md:p-2">
                    <img
                      onClick={() =>
                        setSelectedImage(
                          "https://tecdn.b-cdn.net/img/Photos/Horizontal/Nature/4-col/img%20(72).webp"
                        )
                      }
                      alt="gallery"
                      className="block h-full w-full rounded-lg object-cover object-center"
                      src="https://tecdn.b-cdn.net/img/Photos/Horizontal/Nature/4-col/img%20(72).webp"
                    />
                  </div>
                  <div className="w-full p-1 md:p-2">
                    <img
                      onClick={() =>
                        setSelectedImage(
                          "https://tecdn.b-cdn.net/img/Photos/Horizontal/Nature/4-col/img%20(73).webp"
                        )
                      }
                      alt="gallery"
                      className="block h-full w-full rounded-lg object-cover object-center"
                      src="https://tecdn.b-cdn.net/img/Photos/Horizontal/Nature/4-col/img%20(73).webp"
                    />
                  </div>
                </div>
                <div className="flex w-1/2 flex-wrap">
                  <div className="w-full p-1 md:p-2">
                    <img
                      onClick={() =>
                        setSelectedImage(
                          "https://tecdn.b-cdn.net/img/Photos/Horizontal/Nature/4-col/img%20(74).webp"
                        )
                      }
                      alt="gallery"
                      className="block h-full w-full rounded-lg object-cover object-center"
                      src="https://tecdn.b-cdn.net/img/Photos/Horizontal/Nature/4-col/img%20(74).webp"
                    />
                  </div>
                  <div className="w-1/2 p-1 md:p-2">
                    <img
                      onClick={() =>
                        setSelectedImage(
                          "https://tecdn.b-cdn.net/img/Photos/Horizontal/Nature/4-col/img%20(75).webp"
                        )
                      }
                      alt="gallery"
                      className="block h-full w-full rounded-lg object-cover object-center"
                      src="https://tecdn.b-cdn.net/img/Photos/Horizontal/Nature/4-col/img%20(75).webp"
                    />
                  </div>
                  <div className="w-1/2 p-1 md:p-2">
                    <img
                      onClick={() =>
                        setSelectedImage(
                          "https://tecdn.b-cdn.net/img/Photos/Horizontal/Nature/4-col/img%20(77).webp"
                        )
                      }
                      alt="gallery"
                      className="block h-full w-full rounded-lg object-cover object-center"
                      src="https://tecdn.b-cdn.net/img/Photos/Horizontal/Nature/4-col/img%20(77).webp"
                    />
                  </div>
                </div>
              </div>
            </div>
          </section>
        </div>
      </div>
      <Modal
        isOpen={!!selectedImage}
        title="Image"
        body={<img src={selectedImage} alt="Gallery" />}
        close={() => setSelectedImage(undefined)}
        closeButtonText="Close"
      />
    </>
  );
};
