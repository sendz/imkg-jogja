import React from "react";

type IBanner = {
  image?: string;
  short?: boolean;
};

export const Banner: React.FC<IBanner> = (props) => {
  const { image = require("../assets/images/IMKGYogyaHeader.jpg"), short } =
    props;
  return (
    <div
      className={`w-screen bg-gray-900 bg-center ${
        short ? "h-2/3" : "max-h-screen min-h-screen"
      }`}
      style={{
        background: `url(${image})`,
        backgroundSize: "cover",
        backgroundPosition: "center",
      }}
    >
      {!short && (
        <div
          className="flex h-screen w-screen flex-col items-center justify-center"
          style={{ backdropFilter: "blur(2px)" }}
        >
          <h1 className="text-4xl font-bold text-white">IMKG</h1>
          <h2 className="text-2xl font-bold text-white">Yogyakarta</h2>
        </div>
      )}
    </div>
  );
};
