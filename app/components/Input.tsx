import React, { HTMLProps } from "react";

type IInput = {
  label: string;
  innerRef?: React.Ref<HTMLInputElement>;
} & HTMLProps<HTMLInputElement>;
export const Input: React.FC<IInput> = (props) => {
  const { label, innerRef, className: _className, ...rest } = props;
  return (
    <div className="sm:col-span-3">
      <label
        htmlFor="first-name"
        className="block text-sm font-medium leading-6 text-gray-900"
      >
        {label}
      </label>
      <div className="mt-2">
        <input
          ref={innerRef}
          type="text"
          className={`block w-full rounded-md border-0 px-2 py-1.5 text-gray-900 shadow-sm ring-1 ring-inset ring-gray-300 placeholder:text-gray-400 focus:ring-2 focus:ring-inset focus:ring-indigo-600 sm:text-sm sm:leading-6 ${_className}`}
          {...rest}
        />
      </div>
    </div>
  );
};
