import type { ICoordinate } from "~/components/MapPicker";
import React, { useEffect, useState } from "react";
import { GoogleMap, LoadScript, MarkerF } from "@react-google-maps/api";
import { ClientOnly } from "remix-utils";

type IMapViewer = {
  coordinate: ICoordinate;
  mapsApiKey: string;
};

const containerStyle = {
  width: "100%",
  height: "400px",
};

export const MapViewer: React.FC<IMapViewer> = (props) => {
  const [isLoad, setIsLoad] = useState(false);
  const [coordinate, setCoordinate] = useState<ICoordinate>();
  const [libraries] = useState<any>(["drawing", "places", "visualization"]);

  useEffect(() => {
    if (isLoad) {
      setTimeout(() => {
        setCoordinate(props.coordinate);
      });
    }
  }, [props.coordinate, isLoad]);

  const markerOnLoad = (marker: any) => {
    console.log("marker: ", marker);
  };

  if (!props.coordinate) return null;

  return (
    <ClientOnly>
      {() => (
        <LoadScript
          googleMapsApiKey={props.mapsApiKey}
          libraries={libraries}
          onLoad={() => setTimeout(() => setIsLoad(true))}
        >
          {isLoad && coordinate ? (
            <GoogleMap
              mapContainerStyle={containerStyle}
              center={props.coordinate}
              zoom={15}
            >
              <MarkerF
                onLoad={markerOnLoad}
                position={props.coordinate as any}
              />
            </GoogleMap>
          ) : (
            <div style={containerStyle}>Loading Map</div>
          )}
        </LoadScript>
      )}
    </ClientOnly>
  );
};
