export const formRequest = (
  path: string,
  method: string = "post",
  params: any
) => {
  const form = document.createElement("form");

  form.method = method;
  form.action = path;

  for (const key in params) {
    const input = document.createElement("input");
    input.type = "hidden";
    input.name = key;
    input.value = params[key];
    form.appendChild(input);
  }

  document.body.appendChild(form);
  form.submit();
};
