export type IPaymentHooks = {
  approval_code?: string;
  acquirer?: string;
  eci?: string;
  issuer?: string;
  masked_card?: string;
  card_type?: string;
  va_numbers?: VaNumber[];
  transaction_time: string;
  transaction_status: string;
  transaction_id: string;
  transaction_type?: string;
  status_message: string;
  status_code: string;
  signature_key?: string;
  settlement_time?: string;
  payment_type: string;
  payment_amounts: PaymentAmount[];
  order_id: string;
  merchant_id?: string;
  permata_va_number?: string;
  gross_amount: string;
  fraud_status: string;
  currency: string;
  biller_code?: string;
  bill_key?: string;
  bank?: string;
};

export interface VaNumber {
  va_number: string;
  bank: string;
}

export interface PaymentAmount {
  paid_at: string;
  amount: string;
}
